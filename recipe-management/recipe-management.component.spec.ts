import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from '../app.component';
import { NgModel} from '@angular/forms';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { RecipeManagementComponent } from './recipe-management.component';
import { Component } from '@angular/core';
import { Recipe } from '../Recipe.class';
import { Ingredient } from '../ingredient.class';

describe('recipe-managementComponent', () => {
  let component: RecipeManagementComponent;
  let fixture: ComponentFixture<RecipeManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppComponent, RecipeManagementComponent ], 
      imports: [
        FormsModule
      ],
      providers: []
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe("Testing",() => {

  let recipe : RecipeManagementComponent;
  let fixture: ComponentFixture<RecipeManagementComponent>;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        RecipeManagementComponent
      ],
      imports: [
        FormsModule
      ],
      providers: [],    
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeManagementComponent);
    recipe = fixture.componentInstance;
    fixture.detectChanges();
  });

 

  it("adding a new recipe", () =>{
    let element : HTMLElement = fixture.debugElement.nativeElement; 
    let recName1 = element.querySelector("input.recName");
    let recIng1 = element.querySelector("input.recIngre");
    let recQty1 = element.querySelector("input.recQty");
    let ins1 = element.querySelector("textarea.recInst");

    (<HTMLInputElement>(recName1)).value = "new recipe";
    (<HTMLInputElement>(recIng1)).value = "whatever";
    (<HTMLInputElement>(recQty1)).value = "2";
    (<HTMLInputElement>(ins1)).value = "do this and do that";
    recName1.dispatchEvent(new Event("input"));
    recIng1.dispatchEvent(new Event("input"));
    recQty1.dispatchEvent(new Event("input"));
    ins1.dispatchEvent(new Event("input"));
    fixture.detectChanges();

    let button = element.querySelector("input.createBtn");
    button.dispatchEvent(new Event("click"));
    fixture.detectChanges();

   
    expect(recipe.recipesName[1]).toBe("new recipe")
    expect(recipe.recipesName.length).toBe(2);
    expect(recipe.recipesDetails.length).toBe(2);
    
  });

  it("adding ingredient to existing recipe", () =>{
    let element: HTMLElement = fixture.debugElement.nativeElement;
    let recName1 = element.querySelector("input.recName");
    let recIng1 = element.querySelector("input.recIngre");
    let recQty1 = element.querySelector("input.recQty");

    (<HTMLInputElement>(recName1)).value = "Pie";
    (<HTMLInputElement>(recIng1)).value = "mango";
    (<HTMLInputElement>(recQty1)).value = "8";
   
    recName1.dispatchEvent(new Event("input"));
    recIng1.dispatchEvent(new Event("input"));
    recQty1.dispatchEvent(new Event("input"));
    
    fixture.detectChanges();

    let button = element.querySelector("input.createBtn");
    button.dispatchEvent(new Event("click"));
    fixture.detectChanges();
    console.log(recipe.recipesDetails[0].ingredientList);

    expect(recipe.recipesDetails[0].ingredientList.length).toBe(2);
  })

});

describe("Testing",() => {

  let recipe : RecipeManagementComponent;
  let fixture: ComponentFixture<RecipeManagementComponent>;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        RecipeManagementComponent
      ],
      imports: [
        FormsModule
      ],
      providers: [],    
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeManagementComponent);
    recipe = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("deleting recipe manually",() => {
    recipe.selectedRecipe = recipe.recipesName[0];
    console.log(recipe.selectedRecipe);
    recipe.delete();
    fixture.detectChanges();
    expect(recipe.recipesName.length).toBe(0);
  });

  it("deleting recipe through UI",() => {
    let element : HTMLElement = fixture.debugElement.nativeElement;  
    let recipeList = element.querySelectorAll("td");

    <any>(recipeList[0]).dispatchEvent(new Event("click"));
    fixture.detectChanges();
    recipe.selectedRecipe = <any>recipeList[0];
    fixture.detectChanges();

    let btn = element.querySelector("input.deleteBtn");
    btn.dispatchEvent(new Event("click"));
    fixture.detectChanges();

    expect(recipe.recipesName.length).toBe(0);
   });

  });


  describe("Testing",() => {

    let recipe : RecipeManagementComponent;
    let fixture: ComponentFixture<RecipeManagementComponent>;
    
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [
          AppComponent,
          RecipeManagementComponent
        ],
        imports: [
          FormsModule
        ],
        providers: [],    
      }).compileComponents();
    }));
  
    beforeEach(() => {
      fixture = TestBed.createComponent(RecipeManagementComponent);
      recipe = fixture.componentInstance;
      fixture.detectChanges();
    });

    it("testing editing ingredient", () => {
      let element: HTMLElement = fixture.debugElement.nativeElement;
      let selectIng = element.querySelector("li.testing")
      console.log(selectIng)
      let recIng1 = element.querySelector("input.recIngre");
      //let recQty1 = element.querySelector("input.recQty");

      (<HTMLInputElement>(recIng1)).value ="apples";
    
      //(<HTMLInputElement>(recQty1)).value = "8";
      console.log(selectIng)
      recIng1.dispatchEvent(new Event("input"));
      //recQty1.dispatchEvent(new Event("input"));
      fixture.detectChanges();

      let button = element.querySelector("input.editBtn");
      button.dispatchEvent(new Event("click"));
      fixture.detectChanges();

      
      console.log(recipe.recipesDetails[0].ingredientList[0]);

      //expect(recipe.recipesDetails[0].ingredientList[0].name).toBe("apples");

    })

    it("testing editing", () =>{
      let element: HTMLElement = fixture.debugElement.nativeElement;
      
      let recName1 = element.querySelector("input.recName");
      let recIng1 = element.querySelector("input.recIngre");
      let recQty1 = element.querySelector("input.recQty");
  
      (<HTMLInputElement>(recName1)).value = "Pie";
      (<HTMLInputElement>(recIng1)).value = "mango";
      (<HTMLInputElement>(recQty1)).value = "8";
     
      recName1.dispatchEvent(new Event("input"));
      recIng1.dispatchEvent(new Event("input"));
      recQty1.dispatchEvent(new Event("input"));
      
      fixture.detectChanges();

      let button = element.querySelector("input.createBtn");
      button.dispatchEvent(new Event("click"));
      fixture.detectChanges();

      (<HTMLInputElement>(recIng1)).value = "apple";
      (<HTMLInputElement>(recQty1)).value = "5";
      (<HTMLInputElement>(recName1)).value = "cake";

      recName1.dispatchEvent(new Event("input"));
      recIng1.dispatchEvent(new Event("input"));
      recQty1.dispatchEvent(new Event("input"));
      fixture.detectChanges();

      button.dispatchEvent(new Event("click"));
      fixture.detectChanges();

      let srname = element.querySelector("td.testselect");
      srname.dispatchEvent(new Event("click"));
      fixture.detectChanges();
      let siname = element.querySelector("li.testing");
      siname.dispatchEvent(new Event("click"));
      fixture.detectChanges();
      let sqname = element.querySelector("li.qtytesting");
      sqname.dispatchEvent(new Event("click"));
      fixture.detectChanges();
      

      (<HTMLInputElement>(recName1)).value = "new cake";
      (<HTMLInputElement>(recIng1)).value = "vanilla";
      (<HTMLInputElement>(recQty1)).value = "4";
  

      recName1.dispatchEvent(new Event("input"));
      recIng1.dispatchEvent(new Event("input"));
      recQty1.dispatchEvent(new Event("input"));
    
      fixture.detectChanges();


      let edit = element.querySelector("input.editBtn");
      edit.dispatchEvent(new Event("click"));
      fixture.detectChanges();

      let output = element.querySelector("td.rname");
      let inn = element.querySelector("li.iname");
      let qn = element.querySelector("li.qname");
      
      expect(output.innerHTML).toBe("new cake");
      expect(inn.innerHTML).toBe("vanilla");
      expect(qn.innerHTML).toBe("4");
      
    })

  });
