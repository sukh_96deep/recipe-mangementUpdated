import { Component, OnInit } from '@angular/core';
import {Recipe} from '../Recipe.class';
import { Ingredient } from '../ingredient.class';

@Component({
  selector: 'app-recipe-management',
  templateUrl: './recipe-management.component.html',
  styleUrls: ['./recipe-management.component.css']
})
export class RecipeManagementComponent implements OnInit {

  recipesName: string[] = []; 
  recipesDetails: Recipe[] = [];
  inputName:string = "";
  ingName: string = "";
  ingQty:number = 0;
  inputSelect: string;
  instrcu: string = "";
  time: number = 0;
  numberCount: number = 0;
  selectedRecipe = null;
  selectedIng = null;
  selectedQn = null;
  selectedTime = null;
  selectedInstruction: string = null;
  anewtitle : string = "Recipe Management App";
  
  constructor() { }

  ngOnInit() {
    this.recipesName.push("Pie");
    let recipe0 =new Recipe();
    recipe0.addItem(new Ingredient("apple",7));
    recipe0.addinstrcuction("Peel the apples");
    recipe0.addTime(3);
    this.recipesDetails.push(recipe0);
  }
  

  selectRecipe(inputName){
    this.selectedRecipe = inputName;
  }

  selectIng(recipe, i){
    this.numberCount = i;
    this.selectedIng = recipe.name;
  }

  selectQuantity(recipe){
    this.selectedQn = recipe.quantity;
  }

  selectInstruction(inst){
    this.selectedInstruction = inst;
  }

  selectTime(time){
    this.selectedTime = time;
  }

 
  create(){

    if(!this.recipesName.includes(this.inputName) && this.ingQty >0){

      this.selectedQn = null;
      this.selectedIng = null;
      this.selectedRecipe = null;
      this.selectInstruction = null;
      this.selectedTime = null;

      this.recipesName.push(this.inputName); 
      let recipeEx = new Recipe();
      recipeEx.addItem(new Ingredient(this.ingName, this.ingQty));
      recipeEx.addinstrcuction(this.instrcu);
      recipeEx.addTime(this.time);
      this.recipesDetails.push(recipeEx);
      console.log(this.recipesName) 
      console.log(this.recipesDetails)

    } else if (this.recipesName.includes(this.inputName) && this.ingQty >0){
      this.selectedQn = null;
      this.selectedIng = null;

      let index = this.recipesName.indexOf(this.inputName);
      this.recipesDetails[index].addItem(new Ingredient(this.ingName, this.ingQty));
      
    }
    
  }

 
  delete(){
    if(this.selectedRecipe !== null){
      this.recipesName.splice(this.recipesName.indexOf(this.inputName), 1);
      this.recipesDetails.splice(this.recipesName.indexOf(this.inputName), 1);
    }
   
  }

  edit(){

    for (let rec of this.recipesDetails){

      if (rec.instructionList[0] === this.selectedInstruction && this.instrcu != ""){
        rec.instructionList[0] = this.instrcu;
      }

      for (let recing of rec.ingredientList){
        if (recing.name === this.selectedIng){
          recing.name = this.ingName;
        }

        if (recing.quantity === this.selectedQn && this.ingQty >0){
          recing.quantity = this.ingQty;
        }
      }
    }
    

    this.selectedQn = null;
    this.selectedIng = null;
    this.selectedRecipe = null;
    this.selectedInstruction = null;
    this.selectedTime = null;
  }
    

}

