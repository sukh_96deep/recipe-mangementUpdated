import { Component } from '@angular/core';
import { RecipemanagementService } from './recipemanagement.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(private recservice :RecipemanagementService ){
  }
  ngOnInit(){
    //this.bbtService.retrieveMenu();
    this.recservice.init();
  }

}
