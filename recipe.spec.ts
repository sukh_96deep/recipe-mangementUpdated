/*
    Sukhmandeep Singh Bassi
    100275305
    Assingment#1 - CPSC 2261
**/
import {Recipe} from "./recipe.class"
import { Ingredient } from "./ingredient.class";

describe("recipe testing", function(){
    let time = 1; //1 hour
    let ingreList: Array <Ingredient> = new Array <Ingredient>();
    ingreList.push(new Ingredient("apple", 5));
    ingreList.push(new Ingredient("flour", 1));
    ingreList.push(new Ingredient("butter", 1));

    let instr: Array<String> = new Array<String>();
    instr.push("get the flour");
    instr.push("mix with butter");
    instr.push("bake and it's ready ");

    let pieRecipe = new Recipe();

    // this is initial test
    it("number of ingridents", function(){
        // console.log(ingreList);
        // console.log(instr);
        let numCount = ingreList.length;
        expect(numCount).toBe(3);

    });

 
    // testing if the method added the item by checking it's length
    it("added item", function(){
        let len = pieRecipe.ingredientList.length;
        let sugar = new Ingredient("sugar", 2); 
        pieRecipe.addItem(sugar);
        
        expect(pieRecipe.ingredientList.length).toBe(len+1);
    });

    // testing if the method added the instrcutions by checking it's length
    it("added instrustions", function(){
        let lenInstruc = pieRecipe.instructionList.length;
        let extraStep: Array<String> = new Array<String>();
        pieRecipe.instructionList.push("extra step and eat it now");

        let lengthwithExtra = pieRecipe.instructionList.length;
        expect(pieRecipe.instructionList.length).toBe(lengthwithExtra);
    })

}
)