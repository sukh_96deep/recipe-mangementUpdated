import { Component, OnInit,Input, ViewChild } from '@angular/core';
import { ShoppingList } from '../shoppingList.class';
import { Ingredient } from '../ingredient.class';
import { RecipemanagementService } from '../recipemanagement.service';



@Component({
  selector: 'app-shoppinglist',
  templateUrl: './shoppinglist.component.html',
  styleUrls: ['./shoppinglist.component.css']
})
export class ShoppinglistComponent implements OnInit {
  public service = new RecipemanagementService();
  constructor(service: RecipemanagementService) {
    this.service = service;
   }
    
  ngOnInit() {
  }

}
