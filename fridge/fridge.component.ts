import { Component, OnInit, Input, Output } from '@angular/core';
import { Fridge } from '../fridge.class';
import { Ingredient } from '../ingredient.class';
import { EventEmitter } from 'events';
import { Recipe } from '../recipe.class';
import { RecipemanagementService } from '../recipemanagement.service';

@Component({
  selector: 'app-fridge',
  templateUrl: './fridge.component.html',
  styleUrls: ['./fridge.component.css']
})
export class FridgeComponent implements OnInit {

  public newIng : Ingredient  = new Ingredient("",0);
	public changedIngredient : Ingredient = new Ingredient("",0);
	public selectedIng : Ingredient = null;
	public service = new RecipemanagementService();

	constructor(service : RecipemanagementService){
		this.service = service;	
	}

  
	selectIngredient(i : Ingredient){
  this.selectedIng = i;
  }

  addIngredient(){
    this.service.fridge1.add(this.newIng.name,this.newIng.quantity);
		this.validateQuantity(this.newIng);
		this.service.generateShoppigList();  
  }

  addQuantity(){
    this.selectedIng.add(this.newIng.quantity);
		this.validateQuantity(this.selectedIng);
		this.service.generateShoppigList();  		
  }

  removeQuantity(){
    this.selectedIng.subtract(this.newIng.quantity);
		this.validateQuantity(this.selectedIng);
		this.service.generateShoppigList();  		
  }


  validateQuantity(selectedIngredient : Ingredient){
    if(selectedIngredient.quantity <= 0)
    {
     this.service.fridge1.contents.splice(this.service.fridge1.contents.indexOf(selectedIngredient),1);
      this.selectedIng = null;
    }
  }

  ngOnInit() {
  }

}
